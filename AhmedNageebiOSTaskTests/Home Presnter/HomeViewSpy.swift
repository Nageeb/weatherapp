//
//  HomeViewSpy.swift
//  AhmedNageebiOSTaskTests
//
//  Created by Ahmed Elesawy on 2/14/21.
//

import Foundation
@testable import AhmedNageebiOSTask

class HomeViewSpy: HomeViewProtocol {
    var isLoader = false
    var isReloadTableView = false
    var errorFromServer: String?
    var isFeatchFromLocal = false
    func startLoader() {
        isLoader = true
    }
    
    func stopLoader() {
        isLoader = false
    }
    
    func reloadTableView() {
        isReloadTableView = true
    }
    
    func featchFromLocalStorage() {
        isFeatchFromLocal = true
    }
    
    func responseError() {
        
    }
    
    func showErrorMessageFromServer(with message: String?) {
        errorFromServer = message
    }
    
    func showMessage(with message: String?) {
        
    }
    
    func noInterNet() {
        
    }
}
