//
//  HomePresenterTests.swift
//  AhmedNageebiOSTaskTests
//
//  Created by Ahmed Elesawy on 2/14/21.
//

import XCTest
@testable import AhmedNageebiOSTask
class HomePresenterTests: XCTestCase {
    
    var sut: HomePresenter!
    var viewSpy: HomeViewSpy!
    var repoMock: HomeRepositoryMock!
    override func setUp() {
        super.setUp()
        
        // init
        viewSpy = HomeViewSpy()
        repoMock = HomeRepositoryMock()
        sut = HomePresenter(repo: repoMock, view: viewSpy)
    }
    
    override func tearDown() {
        repoMock = nil
        viewSpy = nil
        sut = nil
        super.tearDown()
    }
    
    func test_Feacth_Weaher() {
        sut.featchWeatherBy(cityName: "Cairo")
        XCTAssert(repoMock.isFeatchWeatherData)
    }
    
    
    func test_Featch_Weather_Fail_FeatchFromLocalStorage() {
        //when
        sut.featchWeatherBy(cityName: "Cairo")
        repoMock.featchFail()
        viewSpy.featchFromLocalStorage()
        
        //Then
        XCTAssert(viewSpy.isFeatchFromLocal)
    }
    
    func test_getCellViewModel() {
        //Given a sut with fetched weathers
        guard let weatherModel = StubGenerator().stubWeathers() else {
            XCTFail("Failed to generate Weather")
            return
        }
        repoMock.weather = weatherModel
        sut.featchWeatherBy(cityName: "Cairo")
        repoMock.featchSuccess()
        
        let testWeather = repoMock.weather?.weather?[0]
        
        //when
        let vm = sut.getCellViewModel(index: 0)
        
        //Then
        XCTAssertEqual(vm.main, testWeather?.main)
    }
    
}
