//
//  StubGenerator.swift
//  AhmedNageebiOSTaskTests
//
//  Created by Ahmed Elesawy on 2/14/21.
//

import Foundation
@testable import AhmedNageebiOSTask
class StubGenerator {
    
    func stubWeathers() -> WeatherDataModel? {
        guard let path = Bundle.tests.path(forResource: "weatherModel", ofType: "json"),
              let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            return nil
        }
        return getWeatherModel(data: data)
    }
    
    private func getWeatherModel(data: Data) -> WeatherDataModel? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        let result = try? decoder.decode(WeatherDataModel.self, from: data)
        return result
    }
}
