//
//  Bundle+TestWeather.swift
//  AhmedNageebiOSTaskTests
//
//  Created by Ahmed Elesawy on 2/14/21.
//

import Foundation
@testable import AhmedNageebiOSTask

extension Bundle {
    public class var tests: Bundle {
        return Bundle(for: NetworkTest.self)
    }
}
