//
//  NetworkMock.swift
//  AhmedNageebiOSTaskTests
//
//  Created by Ahmed Elesawy on 2/14/21.
//

import Foundation
import Alamofire
@testable import AhmedNageebiOSTask

class NetworkMock: NetworkProtocol {
    var isRequestFeatchWeather = false
    
    func request(url: URLRequestConvertible, complitionHandler: @escaping (AppRepsone<WeatherDataModel?>) -> ()) {
        isRequestFeatchWeather = true
    }
}
