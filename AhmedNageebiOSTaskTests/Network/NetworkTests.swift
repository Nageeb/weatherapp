//
//  NetworkTests.swift
//  AhmedNageebiOSTaskTests
//
//  Created by Ahmed Elesawy on 2/14/21.
//

import Foundation

import XCTest
@testable import AhmedNageebiOSTask

class NetworkTest: XCTestCase {
    var sut: Network!
    override func setUp() {
        super.setUp()
        sut = Network()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func test_Request_featch_WeatherByName() {
        // Given
        let promise = XCTestExpectation(description: "Fetch Weather completed")
        var responseError: AppError?
        var responseWeather: [WeatherModel]?
        let bundle = Bundle.tests.path(forResource: "weatherModel", ofType: "json")!
        let url =  URL(fileURLWithPath: bundle)
        let urlRequest = URLRequest(url: url)
       
        // When
        sut.request(url: urlRequest) {(response) in
            switch response {
            case .success(let value):
                responseWeather = value?.weather
            case .error(let error):
                responseError = error
            }
            promise.fulfill()
        }
        wait(for: [promise], timeout: 1)
        
        // Then
        XCTAssertNil(responseError)
        XCTAssertNotNil(responseWeather)
    }
}
