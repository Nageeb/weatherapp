//
//  HomeRepoTests.swift
//  AhmedNageebiOSTaskTests
//
//  Created by Ahmed Elesawy on 2/14/21.
//

import XCTest
@testable import AhmedNageebiOSTask

class HomeRepoTests: XCTestCase {

    var sut: HomeRepositoryProtocol!
    var netWorkMock: NetworkMock!
    
    override func setUp() {
        super.setUp()
        
        netWorkMock = NetworkMock()
        //inject networkMock to repository
        sut = HomeRepository(network: netWorkMock)
    }
    
    override func tearDown() {
        netWorkMock = nil
        sut = nil
        super.tearDown()
    }
    
    func testfeatchWeather() {
        //when
        sut.featchWeather(cityName: "Cairo") { (_) in
        }
        
        //Then
        XCTAssert(netWorkMock.isRequestFeatchWeather)
    }
}

