//
//  HomeRepositoryMock.swift
//  AhmedNageebiOSTaskTests
//
//  Created by Ahmed Elesawy on 2/14/21.
//

import XCTest
@testable import AhmedNageebiOSTask
class HomeRepositoryMock: HomeRepositoryProtocol {
    
    var isFeatchWeatherData = false
    var complition: ((AppRepsone<WeatherDataModel?>) -> ())!
    var weather: WeatherDataModel?
    
    func featchWeather(cityName: String, completionHandler: @escaping (AppRepsone<WeatherDataModel?>) -> ()) {
        isFeatchWeatherData = true
        complition = completionHandler
    }
    
    func featchSuccess() {
        let result = AppRepsone.success(weather)
        complition(result)
        
    }
    func featchFail() {
        let result = AppRepsone<WeatherDataModel?>.error(AppError.internalServerError)
        complition(result)
    }
    
    func saveWeatherDataToLocalStorate(cityName: String, arrWeather: [WeatherListViewModel]) {
        
    }
    
    func getWeatherFromLcoalStorage(cityName: String, complition: @escaping ([WeatherListViewModel]?, AppError?) -> ()) {
        
    }
}
