//
//  LocalStorage.swift
//  Phew
//
//  Created by Ahmed Elesawy on 1/28/21.
//  Copyright © 2021 Ahmed Elesawy. All rights reserved.
//

import Foundation

class WeatherCash {
    
    static var shared = WeatherCash()
    private var cachedData: [String: [WeatherListViewModel]]?
    
    /// get data from cash
    func getCashWeather(key: String , compleation: @escaping ((_ data: [WeatherListViewModel]?) -> Void)) {
        LocalStorageManger.shared.feachDataFromCache([String: [WeatherListViewModel]].self, key: .event) { data in
            guard data != nil else {return compleation(nil)}
            DispatchQueue.main.async {
                
                var localUrl: [WeatherListViewModel]?
                let _ =  data?.filter({ (_key, value) -> Bool in
                    if _key.contains(key) {
                        localUrl = value
                    }
                    return true
                })
                compleation(localUrl)
            }
        }
    }
    
    /// save data to cash
    func addWeaterCash(key: String, model: [WeatherListViewModel], compleation: @escaping (() -> Void)) {
        if cachedData == nil {
            cachedData = [:]
        }
        cachedData?[key] = model
        LocalStorageManger.shared.cache(cachedData, key: .event) {
            DispatchQueue.main.async {
                compleation()
            }
        }
    }
}
