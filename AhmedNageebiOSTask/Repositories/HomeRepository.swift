//
//  HomeRepository.swift
//  AhmedNageebiOSTask
//
//  Created by Ahmed Elesawy on 2/13/21.
//

import Foundation
protocol HomeRepositoryProtocol {
    func featchWeather(cityName: String, completionHandler: @escaping(AppRepsone<WeatherDataModel?>)->())
    func saveWeatherDataToLocalStorate(cityName: String, arrWeather: [WeatherListViewModel])
    func getWeatherFromLcoalStorage(cityName: String, complition: @escaping([WeatherListViewModel]?, AppError?)-> ()) 
    
}

class HomeRepository: HomeRepositoryProtocol {
    private var network:NetworkProtocol
    
    init(network:NetworkProtocol) {
        self.network = network
    }
    
    func featchWeather(cityName: String, completionHandler: @escaping(AppRepsone<WeatherDataModel?>)->()) {
        network.request(url: HomeURLRouter.feacthWarherBy(cityName: cityName), complitionHandler: completionHandler)
    }
    func getWeatherFromLcoalStorage(cityName: String, complition: @escaping([WeatherListViewModel]?, AppError?)-> ()) {
        WeatherCash.shared.getCashWeather(key: cityName) {(arrWeather) in
            if let weathers = arrWeather {
                complition(weathers, nil)
            }else {
                complition(nil, AppError.internalServerError)
            }
        }
    }
    
    func saveWeatherDataToLocalStorate(cityName: String, arrWeather: [WeatherListViewModel]) {
        DispatchQueue.global().async {
            WeatherCash.shared.addWeaterCash(key: cityName, model: arrWeather) {
            }
        }
    }
}
