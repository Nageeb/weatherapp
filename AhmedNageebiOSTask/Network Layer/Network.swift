//
//  Network.swift
//  Base
//
//  Created by Ahmed Elesawy on 4/25/20.
//  Copyright © 2020 Ahmed Elesawy. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol NetworkProtocol {
    func request(url :URLRequestConvertible, complitionHandler : @escaping (AppRepsone<WeatherDataModel?>)->())
}

class Network:NetworkProtocol{
    
    func request(url :URLRequestConvertible, complitionHandler : @escaping (AppRepsone<WeatherDataModel?>)->()){
        AF.request(url).responseJSON { (response) in
            switch response.result {
            case .success(_):
                if let json = response.data{
                    let result : AppRepsone<WeatherDataModel?> = NetworkJson.shared.handelJsonCoder(data: json)
                    complitionHandler(result)
                }else{
                    complitionHandler(.error(.error(response.error?.localizedDescription ?? AppError.internalServerError.description)))
                }
            case .failure(let error):
                let error = ResponseError.filterError(error: error)
                complitionHandler(AppRepsone.error(error))
            }
        }
    }
}
