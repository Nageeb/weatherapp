//
//  BaseCodable.swift
//  Base
//
//  Created by Ahmed Elesawy on 4/25/20.
//  Copyright © 2020 Ahmed Elesawy. All rights reserved.
//

import Foundation
protocol BaseCodable: Codable {
    var status: String { get set }
    var message: String? { get set}
}

struct BaseModel: BaseCodable {
    var status: String
    var message: String?
}

struct BaseModelWith<T: Codable>: BaseCodable {
    var status: String
    var message: String?
    var data: T?
}

struct BaseModelData: BaseCodable {
    var status: String
    var message: String?
    var data :String?
}
