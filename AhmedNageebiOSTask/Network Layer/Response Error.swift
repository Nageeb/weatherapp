//
//  Response Error.swift
//  Base
//
//  Created by Ahmed Elesawy on 11/23/20.
//  Copyright © 2020 Ahmed Elesawy. All rights reserved.
//

import Foundation
import Alamofire

class ResponseError{
    
    static func filterError(error:AFError)->AppError{
        
        if let error =  error as NSError? , error.code == NSURLErrorNotConnectedToInternet {
            return .noInternet
        
        }else if  let error = error as NSError?, error.code == NSURLErrorTimedOut {
            return .timeOut
        }
        else{
            return .noInternet
        }
    }
}
