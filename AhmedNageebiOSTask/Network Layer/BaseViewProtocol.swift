//
//  BaseViewProtocol.swift
//  Zayed Driver
//
//  Created by Ahmed Elesawy on 11/5/19.
//  Copyright © 2019 Ahmed Elesawy. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

protocol BaseViewProtocol: class {
    func showErrorMessageFromServer(with message: String?)
    func showMessage(with message: String?)
    func noInterNet()
}
extension BaseViewProtocol where Self : UIViewController {
    
    func showErrorMessageFromServer(with message: String?){

    }
    
    func showMessage(with message: String?){
        if let _message = message , _message != "" {
            alertButton(message: _message)
        }
    }
    func requestTimeOut() {
        
        present(NoInternetViewController(), animated: true, completion: nil)
    }
    
    func noInterNet() {
        present(NoInternetViewController(), animated: true, completion: nil)
    }
}
