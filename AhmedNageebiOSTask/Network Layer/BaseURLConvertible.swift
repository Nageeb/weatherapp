//
//  BaseURLConvertible.swift
//  Base
//
//  Created by Ahmed Elesawy on 5/4/20.
//  Copyright © 2020 Ahmed Elesawy. All rights reserved.
//

import Alamofire
protocol BaseURLConvertible : URLRequestConvertible{
    var method:HTTPMethod {get}
    var path:String {get}
    var parameter:Parameters?{get}
    var encoding:ParameterEncoding{get}
    var header:HTTPHeaders?{get}
    var baseUrl:String{get}
}

extension BaseURLConvertible {
    var encoding:ParameterEncoding {
        return JSONEncoding.default
    }
    var baseUrl:String{
        return Constance.BaseURL
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let baseURL = URL(string: baseUrl)!
        var urlRequest = URLRequest(url: baseURL.appendingPathComponent(path))
        
        urlRequest.httpMethod = method.rawValue
        
        if let _header = header {
            urlRequest.headers = _header
        }
        return try encoding.encode(urlRequest, with: parameter)
    }
}
