//
//  BasePresenter.swift
//  Base
//
//  Created by Ahmed Elesawy on 5/4/20.
//  Copyright © 2020 Ahmed Elesawy. All rights reserved.
//

import Foundation


class BasePresenter {
    func handelResponse<T:Codable>(view: BaseViewProtocol?, response: AppRepsone<T>) -> T? {
       
        switch response {
        
        case .success(let value):
            return value
            
        case .error(let error):
            checkError(error: error, view: view)
            return nil
        }
        
    }
    
    func checkError(error: AppError, view: BaseViewProtocol?){
        
        switch error {
        
        case .cannotDecode:
            view?.showErrorMessageFromServer(with: error.description)
        case .noInternet:
            view?.noInterNet()
        case .error(let value):
            view?.showErrorMessageFromServer(with: value.description)
            
        case .tokenExpire:
            view?.showErrorMessageFromServer(with: error.description)
        case .validateInputs:
            view?.showErrorMessageFromServer(with: error.description)
        case .badUrl:
            view?.showErrorMessageFromServer(with: error.description)
        case .forbiden:
            view?.showErrorMessageFromServer(with: error.description)
        case .internalServerError:
            view?.showErrorMessageFromServer(with: error.description)
        case .cannotDecodeItem(_):
            view?.showErrorMessageFromServer(with: error.description)
        case .timeOut:
            view?.noInterNet()
        }
    }
}


