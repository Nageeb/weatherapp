//
//  NetworkError.swift
//  Base
//
//  Created by Ahmed Elesawy on 4/25/20.
//  Copyright © 2020 Ahmed Elesawy. All rights reserved.
//

import Foundation


enum NetworkError: Error{
    case faildDecode
    case serverError
    
    var localizedDescription :String {
        switch self {
            
        case .faildDecode:
            return NSLocalizedString("Can not Decode", comment: "eee")
        case .serverError:
            return "Internal Server Error"
        }
        
    }
}
