//
//  ViewController+.swift
//  AhmedNageebiOSTask
//
//  Created by Ahmed Elesawy on 2/13/21.
//

import Foundation
import UIKit
import NVActivityIndicatorView
extension UIViewController{
    
    var toNavigation: UINavigationController {
        let nav = UINavigationController(rootViewController: self)
        return nav
    }
    func alertButton(message:String){
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    @objc func closeKeyboard(){
        view.endEditing(true)
    }
    func showNoInternet(deleget: NoInterneProtocol) {
        let vc = NoInternetViewController()
        vc.deleget = deleget
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: false, completion: nil)
    }
}
