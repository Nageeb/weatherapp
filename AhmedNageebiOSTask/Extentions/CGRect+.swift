//
//  CGRect+.swift
//  AhmedNageebiOSTask
//
//  Created by Ahmed Elesawy on 2/13/21.
//

import Foundation
import UIKit
extension CGRect {
    var center: CGPoint {
        return CGPoint(x: midX, y: midY)
    }
}
