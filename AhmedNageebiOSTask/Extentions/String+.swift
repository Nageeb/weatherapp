//
//  String+.swift
//  AhmedNageebiOSTask
//
//  Created by Ahmed Elesawy on 2/13/21.
//

import Foundation

import Foundation


extension String{
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func toDouble()->Double{
        return Double(self)!
    }
    
    func toInt()->Int{
        return Int(self)!
    }
    
    func toFloat()->Float{
        return Float(self)!
    }
    var trimmedString: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
