//
//  HomeURLRouter.swift
//  AhmedNageebiOSTask
//
//  Created by Ahmed Elesawy on 2/13/21.
//

import Foundation
import Alamofire

enum HomeURLRouter: BaseURLConvertible {
    case feacthWarherBy(cityName: String)
    
    var method: HTTPMethod {
        switch self {
        case .feacthWarherBy:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .feacthWarherBy:
            return ""
        }
    }
    
    var parameter: Parameters? {
        switch self {
        case .feacthWarherBy(let cityName):
            return ["q": cityName, "appid": Constance.apiWeatherKey]
        }
    }
    
    var header: HTTPHeaders? {
        switch self {
        case .feacthWarherBy:
            return nil
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .feacthWarherBy:
            return URLEncoding(destination: .queryString)
        }
    }
}
