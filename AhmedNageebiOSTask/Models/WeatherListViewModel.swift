//
//  WeatherListViewModel.swift
//  AhmedNageebiOSTask
//
//  Created by Ahmed Elesawy on 2/13/21.
//

import Foundation


struct WeatherListViewModel: Codable {
    let main: String
    let desc: String
}
