//
//  NoInternetViewController.swift
//  Base
//
//  Created by Ahmed Elesawy on 12/5/20.
//  Copyright © 2020 Ahmed Elesawy. All rights reserved.
//

import UIKit

protocol NoInterneProtocol: class {
    func reloadData()
}
class NoInternetViewController: UIViewController {
    weak var deleget: NoInterneProtocol?
    @IBOutlet weak var btnReteryAgain: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customeButton()
    }
    private func customeButton() {
        btnReteryAgain.layer.cornerRadius = 15
        btnReteryAgain.layer.borderWidth = 0.4
        btnReteryAgain.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    @IBAction func btnButttonTapped(_ sender: Any) {
        deleget?.reloadData()
        dismiss(animated: false, completion: nil)
    }
}
