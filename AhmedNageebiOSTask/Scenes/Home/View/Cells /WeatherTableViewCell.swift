//
//  WeatherTableViewCell.swift
//  AhmedNageebiOSTask
//
//  Created by Ahmed Elesawy on 2/13/21.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblMain: UILabel!
    @IBOutlet weak var viewContainer: ViewShaow!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        customeViews()
    }
    
    private func customeViews() {
        selectionStyle = .none
        viewContainer.layer.cornerRadius = 5
        viewContainer.clipsToBounds = true
        viewContainer.layer.borderWidth = 0.4
        viewContainer.layer.borderColor = UIColor.white.cgColor
    }
    
    var item: WeatherListViewModel! {
        didSet {
            lblMain.text = item.main
            lblDesc.text = item.desc
        }
    }
}
