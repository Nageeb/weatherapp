//
//  HomeViewController.swift
//  AhmedNageebiOSTask
//
//  Created by Ahmed Elesawy on 2/13/21.
//

import UIKit

class HomeViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtCityName: UITextField!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var lblDataNotAcurate: UILabel!
    
    //MARK:- Variables
    private var presenter: HomePresenterProtocol!
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = HomePresenter(repo: HomeRepository(network: Network()), view: self)
        iniTableView()
        setViews()
    }
    
    //MARK:- Functions
    private func setViews() {
        indicator.isHidden = true
        indicator.layer.cornerRadius = 10
        lblDataNotAcurate.isHidden = true
    }
    //MARK:- Buttons Tapped
    @IBAction func btnSearchTapped(_ sender: Any) {
        self.closeKeyboard()
        presenter.featchWeatherBy(cityName: txtCityName.text)
    }
}

//MARK:- init TableView
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    private func iniTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: WeatherTableViewCell.self)
        tableView.rowHeight = 60
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: WeatherTableViewCell.self, for: indexPath)
        cell.item = presenter.getCellViewModel(index: indexPath.row)
        return cell
    }
}

//MARK:- Delegate for presenter 
extension HomeViewController: HomeViewProtocol, NoInterneProtocol {
    func featchFromLocalStorage() {
        lblDataNotAcurate.isHidden = false
    }
    
    func responseError() {
        showNoInternet(deleget: self)
    }
    
    func reloadData() {
        presenter.featchWeatherBy(cityName: txtCityName.text)
    }
    
    func startLoader() {
        indicator.isHidden = false
        indicator.startAnimating()
    }
    
    func stopLoader() {
        indicator.stopAnimating()
        indicator.isHidden = true
    }
    
    func reloadTableView() {
        lblDataNotAcurate.isHidden = true
        tableView.reloadData()
    }
}
