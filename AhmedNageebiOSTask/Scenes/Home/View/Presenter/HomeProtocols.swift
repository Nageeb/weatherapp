//
//  HomeProtocols.swift
//  AhmedNageebiOSTask
//
//  Created by Ahmed Elesawy on 2/13/21.
//

import Foundation
protocol HomePresenterProtocol {
    func featchWeatherBy(cityName: String?)
    func getCellViewModel(index: Int) -> WeatherListViewModel
    var count: Int { get }
}

protocol HomeViewProtocol: BaseViewProtocol {
    func startLoader()
    func stopLoader()
    func reloadTableView()
    func featchFromLocalStorage()
    func responseError()
}
