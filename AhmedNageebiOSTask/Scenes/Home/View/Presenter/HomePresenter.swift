//
//  HomePresenter.swift
//  AhmedNageebiOSTask
//
//  Created by Ahmed Elesawy on 2/13/21.
//

import Foundation


class HomePresenter: BasePresenter, HomePresenterProtocol {
    
    //MARK:- Varibles
    private weak var view: HomeViewProtocol?
    private var repo: HomeRepositoryProtocol
    private var arrWeathers: [WeatherListViewModel] = []
    
    var count: Int {
        return arrWeathers.count
    }
    /// inject 
    init(repo:HomeRepositoryProtocol, view: HomeViewProtocol) {
        self.view = view
        self.repo = repo
    }
    
    //MARK:- Functions
    func getCellViewModel(index: Int) -> WeatherListViewModel {
        let vm = arrWeathers[index]
        return vm
    }
    
    /// handel model to create vms
    func createCellsViewModel(arrWatherData: [WeatherModel]) {
        arrWeathers.removeAll()
        arrWatherData.forEach({
            let main  = $0.main ?? ""
            let desc  =  $0.weatherDescription ?? ""
            let model = WeatherListViewModel(main: main, desc: desc)
            arrWeathers.append(model)
        })
    }
    
    /// featch Data from remote server
    func featchWeatherBy(cityName: String?) {
        guard let city = cityName, !city.trimmedString.isEmpty else {return}
        view?.startLoader()
        repo.featchWeather(cityName: city) { [weak self](response) in
            self?.view?.stopLoader()
            switch response {
            case .success(let value):
                guard let status = value?.cod?.value else {return}
                
                if status == statusEnum.error.rawValue {
                    self?.getDataFromStorage(cityName: city)
                }else {
                    guard let weathers = value?.weather else {return}
                    self?.successFeatchWeather(arrWeather: weathers, cityName: city)
                }
            case .error(_):
                self?.getDataFromStorage(cityName: city)
            }
        }
    }
    /// handel ui and data when success response
    private func successFeatchWeather(arrWeather: [WeatherModel], cityName: String) {
        createCellsViewModel(arrWatherData: arrWeather)
        view?.reloadTableView()
        repo.saveWeatherDataToLocalStorate(cityName: cityName, arrWeather: arrWeathers)
    }
    
    /// get Data From Local storage when made error response
    private func getDataFromStorage(cityName: String) {
        view?.startLoader()
        repo.getWeatherFromLcoalStorage(cityName: cityName) { [weak self](arrLocal, _) in
            self?.view?.stopLoader()
            if let arrWeather = arrLocal {
                self?.arrWeathers = arrWeather
                self?.view?.reloadTableView()
            }else {
                self?.arrWeathers.removeAll()
                self?.view?.reloadTableView()
                self?.view?.responseError()
            }
        }
    }
}
